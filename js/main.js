$(document).ready(function() {

    $(".nano").nanoScroller({ alwaysVisible: true });

    /* banner__slider */

    if($('.banner__slider').length > 0)	{

        $('.banner__slider').slick({
            fade: true,
            infinite: true,
            dots: true,
            autoplay: true,
            autoplaySpeed: 5000,
            prevArrow: '<div class="banner__arrow banner__prev"></div>',
            nextArrow: '<div class="banner__arrow banner__next"></div>',
            responsive: [
                {
                    breakpoint: 671,
                    settings: {
                        dots: false,
                    }

                },
                {
                    breakpoint: 481,
                    settings: {
                        dots: true,
                        arrows: false
                    }

                }]
        });

    };

    if($('.instagram__slider').length > 0)	{

        $('.instagram__slider').slick({
            infinite: true,
            arrows: false,
            slidesToShow: 5,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 5000,
            responsive: [
                {
                    breakpoint: 1530,
                    settings: {
                        slidesToShow: 4,
                    }

                },
                {
                    breakpoint: 1240,
                    settings: {
                        slidesToShow: 3,
                    }

                },
                {
                    breakpoint: 940,
                    settings: {
                        slidesToShow: 2,
                    }

                },
                {
                    breakpoint: 640,
                    settings: {
                        slidesToShow: 1,
                    }

                }]
        });

    };

    ymaps.ready(init);
    var myMap;

    function init(){

        var myGeocoder = ymaps.geocode("г. Москва, ул. Большая Новодмитровская, 14");
        myGeocoder.then(
            function (res) {
                var point = res.geoObjects.get(0).geometry.getCoordinates();
                myMap = new ymaps.Map("map", {
                    center: point,
                    zoom: 14,
                    controls: []
                });
                var placemark = new ymaps.Placemark(point)
                myMap.geoObjects.add(placemark);
            }
        );
    };

    /* попапы страница сертификатов*/

   $("body").on("click", ".js-prices__btn", function() {
        $(".popups").show();
        $(".prices__popup").show();
        $("body").addClass("popup__open");
        return false;
    });

   $("body").on("click", ".js-others__certificates", function() {
        $(".popups").show();
        $(".certificates__order_popup").show();
        $("body").addClass("popup__open");
        return false;
    });

    $("body").on("click", ".close__btn", function() {
        $(this).closest(".popups").hide();
        $(this).closest(".popup").hide();
        $("body").removeClass("popup__open");
        return false;
    });


    $("body").on("click", function(e) {
        if(!$(".popup").is(e.target) && $(".popup").has(e.target).length === 0) {
            $(".popups").hide();
            $(".popup").hide();
            $("body").removeClass("popup__open");
        };
    });

   /* селект в попапе заказа сертификата */

   $("body").on("click", ".form__item.price input, .form__item.price .arrow", function() {
       $(this).siblings(".submenu").toggle();
       $(".nano").nanoScroller({ alwaysVisible: true });
       return false;
   });

    $("body").on("click", ".certificates__order_popup  .submenu__item", function() {
        $(this).closest(".submenu").siblings("input").val($(this).text());
        $(this).closest(".submenu").hide();
        return false;
    });

    $("body").on("click", function(e) {
        if(!$(".form__item.price").is(e.target) && $(".form__item.price").has(e.target).length === 0) {
            $(".form__item.price").find(".submenu").hide();
        };
    });

    /* слайдеры на странице сертификатов */

    if($(window).width() < 481) {

        $(".advantages__wrap").slick({
            infinite: true,
            prevArrow: '<div class="slider__small_arrow slider__small_prev"></div>',
            nextArrow: '<div class="slider__small_arrow slider__small_next"></div>',
        });

    }

    $(window).resize(function() {
        if($(window).width() < 481 && !$(".advantages__wrap").hasClass("slick-initialized")) {

            $(".advantages__wrap").slick({
                infinite: true,
                prevArrow: '<div class="slider__small_arrow slider__small_prev"></div>',
                nextArrow: '<div class="slider__small_arrow slider__small_next"></div>',
            });

        } else if ($(window).width() >= 481 && $(".advantages__wrap").hasClass("slick-initialized")) {
            $(".advantages__wrap").slick('unslick');
        };
    });


    if($(window).width() < 671) {

        $(".certificate__items").slick({
            infinite: true,
            prevArrow: '<div class="slider__small_arrow slider__small_prev"></div>',
            nextArrow: '<div class="slider__small_arrow slider__small_next"></div>',
        });

    }

    $(window).resize(function() {
        if($(window).width() < 671 && !$(".certificate__items").hasClass("slick-initialized")) {

            $(".certificate__items").slick({
                infinite: true,
                prevArrow: '<div class="slider__small_arrow slider__small_prev"></div>',
                nextArrow: '<div class="slider__small_arrow slider__small_next"></div>',
            });

        } else if ($(window).width() >= 671 && $(".certificate__items").hasClass("slick-initialized")) {
            $(".certificate__items").slick('unslick');
        };
    });

    /* счётчик в корзине */

    $("body").on("click", ".minus__btn", function () {
        var $counter = $(this).closest(".item__info").find(".item__count span");
        var count = parseInt($counter.text()) - 1;
        if(count < 1) {
            count = 1 ;
        }
        $counter.text(count);
        return false;
    });

    $("body").on("click", ".plus__btn", function () {
        var $counter = $(this).closest(".item__info").find(".item__count span");
        $counter.text(parseInt($counter.text()) + 1);
        return false;
    });

    /* слайдеры в корзине */

    if($('.small__slider').length > 0 && $('.big__slider').length > 0)	{
        $('.big__slider').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            draggable: false,
            fade: true,
            asNavFor: '.small__slider'
        });
        $('.small__slider').slick({
            slidesToShow: 4,
            slidesToScroll: 1,
            asNavFor: '.big__slider',
            focusOnSelect: true,
            prevArrow: '<div class="slider__small_arrow slider__small_prev"></div>',
            nextArrow: '<div class="slider__small_arrow slider__small_next"></div>',
            responsive: [
                {
                    breakpoint: 510,
                    settings: {
                        slidesToShow: 3,
                    }

                },
                {
                    breakpoint: 400,
                    settings: {
                        slidesToShow: 2,
                    }

                }]
        });
    };

    /* удаление товара в корзине */

    document.addEventListener("click", function(e) {
        if($(e.target).hasClass("delete__btn")) {
            var index = $(e.target).closest(".small__slide").attr("data-slick-index");
            $('.big__slider').slick('slickRemove', index);
            $(".slider__item").each(function() {
                if($(this).attr("data-slick-index") > index) {
                    $(this).attr("data-slick-index", $(this).attr("data-slick-index") - 1);
                };
            });
            $('.small__slider').slick('slickRemove', index);
            $(".small__slide").each(function() {
                if($(this).attr("data-slick-index") > index) {
                    $(this).attr("data-slick-index", $(this).attr("data-slick-index") - 1);
                };
            });
            return false;
        }
    }, true);

    /* селект в конструкторе */

    $("body").on("click", ".cloth__select .select__field", function() {
        if($(this).parent().hasClass("open")) {
            $(this).siblings(".submenu").hide().parent().removeClass("open");
        } else {
            $(".secondary__tabs_item .select__item").find(".submenu").hide().parent().removeClass("open");
            $(this).siblings(".submenu").show().parent().addClass("open");
        }
        return false;
    });

    $("body").on("click", ".cloth__select .select__item  a", function() {
        $(this).closest(".submenu").siblings(".select__field").text($(this).text());
        $(this).closest(".submenu").hide().parent().removeClass("open");
        return false;
    });

    $("body").on("click", function(e) {
        if(!$(".secondary__tabs_item .select__item").is(e.target) && $(".secondary__tabs_item .select__item").has(e.target).length === 0) {
            $(".secondary__tabs_item .select__item").find(".submenu").hide().parent().removeClass("open");
        };
    });

    $("body").on("click", "form .select__field, form  .arrow", function() {
        if(!$(this).parent().hasClass("open")) {
            $(".select__item").removeClass("open").find(".submenu").hide();
            $(this).parent().addClass("open").find(".submenu").show();
            $(".nano").nanoScroller({ alwaysVisible: true });
        } else {
            $(this).parent().removeClass("open").find(".submenu").hide();
            $(".nano").nanoScroller({ alwaysVisible: true });
        };
        return false;
    });

    $("body").on("click", "form .submenu  a", function() {
        $(this).closest(".submenu").siblings(".select__field").val($(this).text());
        $(this).closest(".submenu").hide().parent().removeClass("open");
        return false;
    });

    $("body").on("click", function(e) {
        if(!$("form .select__item").is(e.target) && $("form .select__item").has(e.target).length === 0) {
            $("form .select__item").removeClass("open").find(".submenu").hide();
        };
    });

    /* слайдеры в конструкторе */

    if($(".constructor__options").hasClass("constructor__slider")) {
        $('.constructor__options.constructor__slider').slick({
            infinite: true,
            slidesToShow: 4,
            slidesToScroll: 1,
            prevArrow: '<div class="slider__small_arrow slider__small_prev"></div>',
            nextArrow: '<div class="slider__small_arrow slider__small_next"></div>',

        });
    }

    if($('.popup__slider').length > 0) {

        $('.popup__slider').slick({
            fade: true,
            infinite: true,
            prevArrow: '<div class="banner__arrow banner__prev"></div>',
            nextArrow: '<div class="banner__arrow banner__next"></div>',
        });

    }

    /* блоки картинок в снятии мерок */

    $("body").on("focus", ".measure__form input", function() {
       var dataMeasure = $(this).attr("data-measure");
       if($(".measure__imgs[data-measure=" + dataMeasure + "]").attr("data-measure") && !$(".measure__imgs[data-measure=" + dataMeasure + "]").hasClass("open")) {
           $(".measure__imgs").removeClass("open");
           $(".measure__imgs[data-measure=" + dataMeasure + "]").addClass("open");
       }
    });

    /* попапы страница конструктора*/

    $("body").on("click", ".js-cloth__btn", function() {
        $(".popups").show();
        $(".cloth__popup").show();
        $("body").addClass("popup__open");
        return false;
    });

    $("body").on("click", ".js-book__btn", function() {
        $(".popups").show();
        $(".book__popup").show();
        $("body").addClass("popup__open");
        return false;
    });

    /* попапы страница контактов*/

    $("body").on("click", ".js-partnership__btn", function() {
        $(".popups").show();
        $(".partnership__popup").show();
        $("body").addClass("popup__open");
        return false;
    });

    $("body").on("change", "#form_file", function() {
        $(".added__file").text("" + $(this).val().split('\\')[$(this).val().split('\\').length - 1] + "");
    });

    /* зум в конструкторе */

    $(".zoom__btn").fancybox();

    /* табы в конструкторе */

    $("body").on("click", ".primary__tabs_link", function() {
        var link = $(this).attr("href");
        if(!$(this).hasClass("active")) {
            $(".primary__tabs_link").removeClass("active");

            $(this).addClass("active");

            $(".primary__tabs_item").removeClass("active");

            $(".secondary__tabs_link").removeClass("active");

            if($(link).find(".secondary__tabs_link")) {

                $(link).find(".secondary__tabs_link").eq(0).addClass("active");

            }

            $(".secondary__tabs_item").removeClass("active");

            $(link).addClass("active").find(".secondary__tabs_item").eq(0).addClass("active");

            $('.constructor__options.constructor__slider').slick('setPosition');
        }

        if($("#t2s1").hasClass("active")) {
            $(".constructor__item").find(".item__details_img").addClass("active");
            $(".constructor__item").find(".embroidery__style").removeClass("active");
            $(".constructor__item").find(".button__style").addClass("active");
        } else if ($("#t2s2").hasClass("active")) {
            $(".constructor__item").find(".item__details_img").addClass("active");
            $(".constructor__item").find(".button__style").removeClass("active");
            $(".constructor__item").find(".embroidery__style").addClass("active");
        } else if(!$("#t2s1").hasClass("active") && !$("#t2s2").hasClass("active") && $(".constructor__item").find(".item__details_img img").hasClass("active")) {
            $(".constructor__item").find(".item__details_img").removeClass("active");
            $(".constructor__item").find(".button__style").removeClass("active");
            $(".constructor__item").find(".embroidery__style").removeClass("active");
        };

        return false;
    });

    $("body").on("click", ".secondary__tabs_link", function() {
        var link = $(this).attr("href");
        if(!$(this).hasClass("active")) {
            $(".secondary__tabs_link").removeClass("active");

            $(this).addClass("active");

            $(".secondary__tabs_item").removeClass("active");

            $(link).addClass("active");

            $('.constructor__options.constructor__slider').slick('setPosition');
        }

        if($("#t2s1").hasClass("active")) {
            $(".constructor__item").find(".item__details_img").addClass("active");
            $(".constructor__item").find(".embroidery__style").removeClass("active");
            $(".constructor__item").find(".button__style").addClass("active");
        } else if ($("#t2s2").hasClass("active")) {
            $(".constructor__item").find(".item__details_img").addClass("active");
            $(".constructor__item").find(".button__style").removeClass("active");
            $(".constructor__item").find(".embroidery__style").addClass("active");
        } else if(!$("#t2s1").hasClass("active") && !$("#t2s2").hasClass("active") && $(".constructor__item").find(".item__details_img img").hasClass("active")) {
            $(".constructor__item").find(".item__details_img").removeClass("active");
            $(".constructor__item").find(".button__style").removeClass("active");
            $(".constructor__item").find(".embroidery__style").removeClass("active");
        };

        return false;
    });

    $("body").on("click", ".js-prev__step_btn", function() {

        if($(".secondary__tabs_item.active").prev().hasClass("secondary__tabs_item")) {

            $(".secondary__tabs_link.active").removeClass("active").prev(".secondary__tabs_link").addClass("active");
            $(".secondary__tabs_item.active").removeClass("active").prev(".secondary__tabs_item").addClass("active");
            $('.constructor__options.constructor__slider').slick('setPosition');

        } else if($(".primary__tabs_item.active").prev().hasClass("primary__tabs_item")) {

            $(".secondary__tabs_link").removeClass("active");
            $(".secondary__tabs_item").removeClass("active");
            $(".primary__tabs_link.active").removeClass("active").prev(".primary__tabs_link").addClass("active");
            $(".primary__tabs_item.active").removeClass("active").prev(".primary__tabs_item").addClass("active");
                $(".primary__tabs_item.active").find(".secondary__tabs_link").last().addClass("active");
            $(".primary__tabs_item.active").find(".secondary__tabs_item").last().addClass("active");
            $('.constructor__options.constructor__slider').slick('setPosition');

        };

        if($("#t2s1").hasClass("active")) {
            $(".constructor__item").find(".item__details_img").addClass("active");
            $(".constructor__item").find(".embroidery__style").removeClass("active");
            $(".constructor__item").find(".button__style").addClass("active");
        } else if ($("#t2s2").hasClass("active")) {
            $(".constructor__item").find(".item__details_img").addClass("active");
            $(".constructor__item").find(".button__style").removeClass("active");
            $(".constructor__item").find(".embroidery__style").addClass("active");
        } else if(!$("#t2s1").hasClass("active") && !$("#t2s2").hasClass("active") && $(".constructor__item").find(".item__details_img img").hasClass("active")) {
            $(".constructor__item").find(".item__details_img").removeClass("active");
            $(".constructor__item").find(".button__style").removeClass("active");
            $(".constructor__item").find(".embroidery__style").removeClass("active");
        };

        return false;
    });

    $("body").on("click", ".js-next__step_btn", function() {

        if($(".secondary__tabs_item.active").next().hasClass("secondary__tabs_item")) {

            $(".secondary__tabs_link.active").removeClass("active").next(".secondary__tabs_link").addClass("active");
            $(".secondary__tabs_item.active").removeClass("active").next(".secondary__tabs_item").addClass("active");
            $('.constructor__options.constructor__slider').slick('setPosition');

        } else if($(".primary__tabs_item.active").next().hasClass("primary__tabs_item")) {

            $(".secondary__tabs_link").removeClass("active");
            $(".secondary__tabs_item").removeClass("active");
            $(".primary__tabs_link.active").removeClass("active").next(".primary__tabs_link").addClass("active");
            $(".primary__tabs_item.active").removeClass("active").next(".primary__tabs_item").addClass("active");
                $(".primary__tabs_item.active").find(".secondary__tabs_link").eq(0).addClass("active");
            $(".primary__tabs_item.active").find(".secondary__tabs_item").eq(0).addClass("active");
            $('.constructor__options.constructor__slider').slick('setPosition');

        };

        if($("#t2s1").hasClass("active")) {
            $(".constructor__item").find(".item__details_img").addClass("active");
            $(".constructor__item").find(".embroidery__style").removeClass("active");
            $(".constructor__item").find(".button__style").addClass("active");
        } else if ($("#t2s2").hasClass("active")) {
            $(".constructor__item").find(".item__details_img").addClass("active");
            $(".constructor__item").find(".button__style").removeClass("active");
            $(".constructor__item").find(".embroidery__style").addClass("active");
        } else if(!$("#t2s1").hasClass("active") && !$("#t2s2").hasClass("active") && $(".constructor__item").find(".item__details_img img").hasClass("active")) {
            $(".constructor__item").find(".item__details_img").removeClass("active");
            $(".constructor__item").find(".button__style").removeClass("active");
            $(".constructor__item").find(".embroidery__style").removeClass("active");
        };

        return false;
    });

    $("body").on("click", ".js-constructor__option", function() {
        if(!$(this).hasClass("active")) {
            $(this).parent().find(".js-constructor__option").removeClass("active");
            $(this).addClass("active");
        }
    });

    /* мобильное меню */

    $("body").on("click", ".menu__btn", function() {
        $(".wrapper").addClass("active");
        return false;
    });

    $("body").on("click", ".close__btn_mob", function() {
        $(".wrapper").removeClass("active");
        return false;
    });

    $(window).resize(function() {
        if($(window).width() > 670) {
            $(".wrapper").removeClass("active");
        }
    });

    $("body").on("click", ".mobile__menu .menu__link", function() {
        if($(this).hasClass("active")) {
            $(this).removeClass("active").siblings(".submenu").slideUp("normal");
        } else {
            $(".mobile__menu").find(".menu__link").removeClass("active").siblings(".submenu").slideUp("normal");
            $(this).addClass("active").siblings(".submenu").slideDown("normal");
        }
        return false;
    });

    /* боковое меню на странице блога */

    $("body").on("click",".slide-menu__btn", function() {
        $(this).closest(".blog__slide-menu").toggleClass("open");
        return false;
    });

    $("body").on("click",".blog__slide-menu .menu__head", function() {
        $(this).siblings(".submenu").slideToggle("normal",function() {
            if($(".blog__slide-menu").innerHeight() >= $(".blog__slide-menu").find(".menu__items").innerHeight()) {
                $(".blog__menu_btn").height($(".blog__slide-menu").innerHeight());
            } else {
                $(".blog__menu_btn").height($(".blog__slide-menu").find(".menu__items").innerHeight());
            }
        });
        return false;

    });

    $(window).resize(function() {
        if($(".blog__slide-menu").innerHeight() >= $(".blog__slide-menu").find(".menu__items").innerHeight()) {
            $(".blog__menu_btn").height($(".blog__slide-menu").innerHeight());
        } else {
            $(".blog__menu_btn").height($(".blog__slide-menu").find(".menu__items").innerHeight());
        }
    });

    /* попап на странице блога */

    $("body").on("click", ".js-subscription__btn", function() {
        $(".popups").show();
        $(".subscribe__popup").show();
        $("body").addClass("popup__open");
        return false;
    });

    /* попапы на детальной странице блога */

    $("body").on("click", ".js-add__comment_btn", function() {
        $(".popups").show();
        $(".comments__popup").show();
        $("body").addClass("popup__open");
        return false;
    });

    $("body").on("click", ".js-subscription__btn", function() {
        $(".popups").show();
        $(".subscribe__popup").show();
        $("body").addClass("popup__open");
        return false;
    });

});
